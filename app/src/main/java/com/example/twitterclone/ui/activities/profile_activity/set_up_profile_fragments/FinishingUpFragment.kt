package com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.example.twitterclone.R
import kotlinx.android.synthetic.main.fragment_finishing_up.*

class FinishingUpFragment : Fragment() {

    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inflater.inflate(R.layout.fragment_finishing_up, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as SetUpProfileActivity?)!!.setSupportActionBar(toolbar)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )

        seeProfileButton.setOnClickListener {
            val intent = Intent(requireActivity(), SetUpProfileActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent)
            requireActivity().finish()
        }

    }

}