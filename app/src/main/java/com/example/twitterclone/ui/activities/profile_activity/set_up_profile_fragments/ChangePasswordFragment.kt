package com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_change_password.*


class ChangePasswordFragment : Fragment() {

    private var hiddenInput = true
    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inflater.inflate(R.layout.fragment_change_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as SetUpProfileActivity?)!!.setSupportActionBar(toolbar)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )

        newPasswordEditText.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (p1?.action == MotionEvent.ACTION_UP) {
                    if (p1.rawX >= newPasswordEditText.right - newPasswordEditText.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        // your action here
                        if (hiddenInput) {
                            newPasswordEditText.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            hiddenInput = false
                        } else {
                            newPasswordEditText.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                            hiddenInput = true
                        }
                        return true
                    }
                }
                return false
            }
        })

        newPasswordEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    updatePasswordOnParse(newPasswordEditText.text.toString())
                    return true
                }
                return false
            }
        })

        nextButton.setOnClickListener {
            val currentUsername = currentUsernameEditText.text.toString()
            val newPassword = newPasswordEditText.text.toString()
            val query = ParseUser.getQuery()
            query.whereEqualTo("username", ParseUser.getCurrentUser().username)
            query.findInBackground { results, e ->
                // results has the list of users that match either the email address or username
                if (e == null && results.size > 0) {
                    if (currentUsername == results[0].username && newPassword.isNotEmpty()) {
                        currentUsernameEditText.error = null
                        newPasswordEditText.error = null
                        updatePasswordOnParse(newPassword)
                    } else if (currentUsername != results[0].username && newPassword.isNotEmpty()) {
                        currentUsernameEditText.error = "Current username is not correct."
                        newPasswordEditText.error = null
                    } else {
                        if (currentUsername.isEmpty() && newPassword.isNotEmpty()) {
                            currentUsernameEditText.error =
                                "Fill this field if you want to update your password"
                            newPasswordEditText.error = null
                        } else if (currentUsername.isNotEmpty() && newPassword.isEmpty()) {
                            newPasswordEditText.error =
                                "Fill this field if you want to update your password"
                            currentUsernameEditText.error = null
                        } else {
                            NavHostFragment.findNavController(this)
                                .navigate(R.id.action_changePasswordFragment_to_finishingUpFragment)
                            inputMethodManager.hideSoftInputFromWindow(
                                requireActivity().currentFocus?.windowToken,
                                0
                            )
                        }

                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        "If you see this message something is wrong with server",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        skipForNowTV.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.action_changePasswordFragment_to_finishingUpFragment)
        }

        changePasswordFragment.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }
    }

    private fun updatePasswordOnParse(newPassword: String) {
        val parseUser = ParseUser.getCurrentUser()
        parseUser.setPassword(newPassword)
        parseUser.saveInBackground { e ->
            if (e == null) {
                Toast.makeText(requireActivity(), "Password was updated.", Toast.LENGTH_SHORT)
                    .show()
                NavHostFragment.findNavController(this)
                    .navigate(R.id.action_changePasswordFragment_to_finishingUpFragment)
            } else {
                Toast.makeText(requireActivity(), "Couldn't update password.", Toast.LENGTH_SHORT).show()
            }
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
    }
}