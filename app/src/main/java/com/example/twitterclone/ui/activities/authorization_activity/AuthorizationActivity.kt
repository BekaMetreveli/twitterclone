package com.example.twitterclone.ui.activities.authorization_activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.twitterclone.R

class AuthorizationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authorization)
    }
}