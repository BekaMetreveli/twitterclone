package com.example.twitterclone.ui.activities.authorization_activity.authorization_fragments

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.*
import androidx.fragment.app.Fragment
import android.view.inputmethod.InputMethodManager
import androidx.navigation.fragment.navArgs
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.authorization_activity.AuthorizationActivity
import kotlinx.android.synthetic.main.fragment_forgot_password.toolbar
import kotlinx.android.synthetic.main.fragment_new_password.*

class NewPasswordFragment : Fragment() {

    val args: NewPasswordFragmentArgs by navArgs()
    private var hiddenInput = true
    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().setTheme(R.style.CreateAccountFragmentTheme)
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inflater.inflate(R.layout.fragment_new_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AuthorizationActivity?)!!.setSupportActionBar(toolbar)
        (activity as AuthorizationActivity?)!!.supportActionBar?.title =
            resources.getString(R.string.update_password)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )

        newPasswordEditText.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (p1?.action == MotionEvent.ACTION_UP) {
                    if (p1.rawX >= newPasswordEditText.right - newPasswordEditText.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        // your action here
                        if (hiddenInput) {
                            newPasswordEditText.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            hiddenInput = false
                        } else {
                            newPasswordEditText.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                            hiddenInput = true
                        }
                        return true
                    }
                }
                return false
            }
        })

        newPasswordEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    updatePasswordOnParse(args.username)
                    return true
                }
                return false
            }
        })

        updateButton.setOnClickListener {
            updatePasswordOnParse(args.username)
        }

        updatePasswordFragment.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }
    }


    private fun updatePasswordOnParse(username: String) {
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
        //TODO update password with email

    }
}