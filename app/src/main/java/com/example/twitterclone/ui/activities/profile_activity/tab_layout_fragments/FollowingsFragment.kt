package com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments.adapters.FollowingsRecyclerViewAdapter
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_followings.*

class FollowingsFragment : Fragment() {

    private lateinit var adapter: FollowingsRecyclerViewAdapter
    private var users = mutableListOf<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        getUsers()
    }

    private fun setAdapter() {
        adapter = FollowingsRecyclerViewAdapter(emptyList())
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
    }

    private fun getUsers() {
        val tempUsers = ParseUser.getCurrentUser().getList<String>("isFollowing") as MutableList<String>?
        tempUsers?.let { users.addAll(it) }
        adapter.setAdapterData(users)
    }
}