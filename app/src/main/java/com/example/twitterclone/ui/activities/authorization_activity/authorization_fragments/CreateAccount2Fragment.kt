package com.example.twitterclone.ui.activities.authorization_activity.authorization_fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.authorization_activity.AuthorizationActivity
import com.example.twitterclone.ui.activities.twitter_activity.TwitterActivity
import com.parse.ParseObject
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_create_account2.*
import kotlinx.android.synthetic.main.fragment_create_account2.view.*

class CreateAccount2Fragment : Fragment() {

    val args: CreateAccount2FragmentArgs by navArgs()
    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().setTheme(R.style.CreateAccountFragmentTheme)
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_create_account2, container, false)
        view.userNameTV.text = args.userName
        view.dateOfBirthTV.text = args.dateOfBirth
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AuthorizationActivity?)!!.setSupportActionBar(toolbar)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        signUpButton.setOnClickListener {
            userSignUp(args.userName, args.password, args.dateOfBirth)

        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
    }

    private fun userSignUp(userName: String, password: String, dateOfBirth: String) {
        val user = ParseUser()
        user.username = userName
        user.setPassword(password)
        user.put("dateOfBirth", dateOfBirth)
        user.signUpInBackground { e ->
            if (e == null) {
                //Ok
                Log.i("Sign Up", "Successful")
                val obj = ParseObject("Followers")
                obj.put("influencer", user.username)
                obj.saveInBackground { e ->
                    if (e == null) {
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(requireActivity(), TwitterActivity::class.java))
                        requireActivity().finish()
                    } else {
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Log.i("Sign Up", e.message.toString())
                Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
            }
        }

    }

}