package com.example.twitterclone.ui.activities.twitter_activity.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.twitterclone.R
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : Fragment() {

    private lateinit var searchViewModel: SearchViewModel

    private lateinit var adapter: UsersRecyclerViewAdapter
    private var users = mutableListOf<User>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        searchViewModel =
            ViewModelProvider(this).get(SearchViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_search, container, false)
//        val textView: TextView = root.findViewById(R.id.text_dashboard)
//        searchViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        getUsers()
    }

    private fun setAdapter() {
        adapter = UsersRecyclerViewAdapter(emptyList())
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
    }

    private fun getUsers() {
        val query = ParseUser.getQuery()
        query.whereNotEqualTo("username", ParseUser.getCurrentUser().username)
        query.addAscendingOrder("username")
        query.findInBackground { objects, e ->
            if (e == null) {
                if (objects.size > 0) {
                    for (user in objects) {
                        val newUser = User(
                            user.username,
                            user.get("dateOfBirth") as String?,
                            user.get("Bio") as String?,
                            user.get("Address") as String?
                        )
                        users.add(newUser)
                    }
                    adapter.setAdapterData(users)
                }
            } else {
                e.printStackTrace()
            }
        }
    }
}