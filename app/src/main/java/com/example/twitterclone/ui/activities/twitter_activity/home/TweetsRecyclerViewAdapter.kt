package com.example.twitterclone.ui.activities.twitter_activity.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.twitterclone.R
import kotlinx.android.synthetic.main.adapter_tweets.view.*


class TweetsRecyclerViewAdapter(private var users: List<String>, private var tweets: List<String>) :
    RecyclerView.Adapter<TweetsRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var user: String
        private lateinit var tweet: String
        fun onBind() {
            user = users[adapterPosition]
            tweet = tweets[adapterPosition]
            itemView.userName.text = user
            itemView.tweetTextView.text = tweet
        }
    }

    fun setAdapterData(users: List<String>, tweets: List<String>) {
        this.users = users
        this.tweets = tweets
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.adapter_tweets, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.onBind()

    override fun getItemCount(): Int = tweets.size
}