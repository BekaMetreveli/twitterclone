package com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.parse.ParseFile
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_pick_a_profile_picture.*
import kotlinx.android.synthetic.main.fragment_pick_a_profile_picture.view.*
import java.io.ByteArrayOutputStream
import java.lang.Exception

class PickAProfilePictureFragment : Fragment() {

    companion object {
        private const val TAKE_PHOTO_REQUEST_CODE = 0
        private const val CHOOSE_FROM_GALLERY_REQUEST_CODE = 1
    }

    private lateinit var itemView: View
    private var chosenImage: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_pick_a_profile_picture, container, false)
        itemView = view
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as SetUpProfileActivity?)!!.setSupportActionBar(toolbar)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        skipForNowTV.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.openPickAHeaderFragment)
        }

        nextButton.setOnClickListener {
            if (chosenImage != null) {
                saveImageOnParse(chosenImage)
            } else {
                NavHostFragment.findNavController(this).navigate(R.id.openPickAHeaderFragment)
            }
        }
        takeAPictureLayout.setOnClickListener {
            checkPermissions()

        }

    }

    private fun checkPermissions() {
        if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)
        } else {
            selectImage(requireContext())
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage(requireContext())
            }
        }
    }

    private fun selectImage(context: Context) {
        val options = arrayOf<CharSequence>("Take Photo", "Choose from Gallery", "Cancel")
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle("Choose your profile picture")
        builder.setItems(options, DialogInterface.OnClickListener { dialog, item ->
            when {
                options[item] == "Take Photo" -> {
                    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(takePicture, TAKE_PHOTO_REQUEST_CODE)
                }
                options[item] == "Choose from Gallery" -> {
                    val pickPhoto =
                        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    startActivityForResult(pickPhoto, CHOOSE_FROM_GALLERY_REQUEST_CODE)
                }
                options[item] == "Cancel" -> {
                    dialog.dismiss()
                }
            }
        })
        builder.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != AppCompatActivity.RESULT_CANCELED) {
            when (requestCode) {
                TAKE_PHOTO_REQUEST_CODE -> if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val selectedImage = data.extras!!["data"] as Bitmap?
                    itemView.img_profile.setImageBitmap(selectedImage)
                    chosenImage = getImageUri(requireContext(), selectedImage)
                }
                CHOOSE_FROM_GALLERY_REQUEST_CODE -> if (resultCode == AppCompatActivity.RESULT_OK && data != null) {
                    val selectedImage: Uri? = data.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    if (selectedImage != null) {
                        val cursor: Cursor? = requireContext().contentResolver.query(
                            selectedImage,
                            filePathColumn, null, null, null
                        )
                        if (cursor != null) {
                            cursor.moveToFirst()
                            val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                            val picturePath: String = cursor.getString(columnIndex)
                            itemView.img_profile.setImageBitmap(BitmapFactory.decodeFile(picturePath))
                            cursor.close()
                        }
                    }
                }
            }
        }
    }

    private fun getImageUri(inContext: Context, inImage: Bitmap?): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    private fun saveImageOnParse(chosenImage: Uri?) {
        try {
            val bitmap = MediaStore.Images.Media.getBitmap(
                requireActivity().contentResolver,
                chosenImage
            )
            val stream = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)

            val byteArray = stream.toByteArray()
            val file = ParseFile("profilePicture.png", byteArray)

            val obj = ParseUser.getCurrentUser()
            obj.put("profilePicture", file)

            obj.saveInBackground { e ->
                if (e == null) {
                    Toast.makeText(requireActivity(), "Image was uploaded", Toast.LENGTH_SHORT)
                        .show()
                    NavHostFragment.findNavController(this).navigate(R.id.openPickAHeaderFragment)
                } else {
                    Toast.makeText(requireActivity(), "Couldn't upload image.", Toast.LENGTH_SHORT).show()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}