package com.example.twitterclone.ui.activities.authorization_activity.authorization_fragments

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.authorization_activity.AuthorizationActivity
import kotlinx.android.synthetic.main.fragment_create_account.*
import kotlinx.android.synthetic.main.fragment_create_account.passwordET
import kotlinx.android.synthetic.main.fragment_create_account.passwordTextInputLayout
import kotlinx.android.synthetic.main.fragment_create_account.toolbar
import java.util.*


class CreateAccountFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().setTheme(R.style.CreateAccountFragmentTheme)
        val view = inflater.inflate(R.layout.fragment_create_account, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AuthorizationActivity?)!!.setSupportActionBar(toolbar)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        logInTextView.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.openLogInFragmentFromCreateAccountFragment)
        }

        dateOfBirthTextView.setOnClickListener {
            val c = Calendar.getInstance()
            val mYear = c[Calendar.YEAR]
            val mMonth = c[Calendar.MONTH]
            val mDay = c[Calendar.DAY_OF_MONTH]

            val datePickerDialog = DatePickerDialog(
                requireActivity(), R.style.MySpinnerDatePickerStyle,
                OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    dateOfBirthTextView.text = resources.getString(
                        R.string.date_of_birth_input,
                        dayOfMonth,
                        monthOfYear + 1,
                        year
                    )
                }, mYear, mMonth, mDay
            )
            datePickerDialog.show()
        }

        passwordET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    checkValidity(view)
                    return true
                }
                return false
            }
        })

        nextButton.setOnClickListener {
            checkValidity(view)
        }

    }

    private fun checkValidity(view: View) {
        val userName = nameET.text.toString()
        val password = passwordET.text.toString()
        val userNameLength = 15

        if (userName.length <= userNameLength && password.isNotEmpty()) {
            var dateOfBirth = "Date of birth"
            if (dateOfBirthTextView.text.toString() != "Date of birth") {
                dateOfBirth = dateOfBirthTextView.text.toString()
            }
            val action = CreateAccountFragmentDirections.openCreateAccount2Fragment(nameET.text.toString(), dateOfBirth, passwordET.text.toString())
            Navigation.findNavController(view).navigate(action)
        } else {
            if (userName.isEmpty()) {
                nameTextInputLayout.error = "Enter a valid username"
                nameET.error = "Please fill out this field"
            }
            if (password.isEmpty()) {
                passwordTextInputLayout.error = "Enter a valid password"
                passwordET.error = "Please fill out this field"
            }
        }
    }


}