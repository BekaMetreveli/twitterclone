package com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.google.android.gms.location.*

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_maps.*
import kotlinx.android.synthetic.main.fragment_maps.toolbar

class MapsFragment : Fragment() {

    companion object {
        private const val REQUEST_LOCATION = 11
        private const val TAG = "MapsActivity"
    }

    private lateinit var inputMethodManager: InputMethodManager

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var map: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        setupLocationClient()
        requestLocationPermissions()
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        (activity as SetUpProfileActivity?)!!.setSupportActionBar(toolbar)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )

        skipForNowTV.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.action_mapsFragment_to_changePasswordFragment)
        }

        val parseAddress = ParseUser.getCurrentUser().get("Address") as String?
        if (parseAddress?.isNotEmpty() == true) {
            locationEditText.setText(parseAddress)
        }
        nextButton.setOnClickListener {
            val currentAddress = locationEditText.text.toString()
            if (currentAddress != parseAddress) {
                saveLocationOnParse(currentAddress)
            } else {
                NavHostFragment.findNavController(this).navigate(R.id.action_mapsFragment_to_changePasswordFragment)
            }
        }

        locationEditText.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (p1?.action == MotionEvent.ACTION_UP) {
                    if (p1.rawX >= locationEditText.right - locationEditText.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        // your action here
                        getCurrentLocation()
                        return true
                    }
                }
                return false
            }
        })

        locationEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    val currentAddress = locationEditText.text.toString()
                    if (currentAddress != parseAddress) {
                        saveLocationOnParse(currentAddress)
                    } else {
                        NavHostFragment.findNavController(this@MapsFragment).navigate(R.id.action_mapsFragment_to_changePasswordFragment)
                    }
                    return true
                }
                return false
            }
        })

        mapsFragment.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }

    }

    private fun saveLocationOnParse(address: String) {
        val obj = ParseUser.getCurrentUser()
        obj.put("Address", address)

        obj.saveInBackground { e ->
            if (e == null) {
                Toast.makeText(requireActivity(), "Address was updated.", Toast.LENGTH_SHORT).show()
                NavHostFragment.findNavController(this).navigate(R.id.action_mapsFragment_to_changePasswordFragment)
            } else {
                Toast.makeText(requireActivity(), "Couldn't update location.", Toast.LENGTH_SHORT).show()
            }
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
    }

    private val callback = OnMapReadyCallback { googleMap ->
        map = googleMap
        getCurrentLocation()
        map.setOnPoiClickListener {
            locationEditText.setText(it.name)
        }
    }

    private fun setupLocationClient() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
    }


    private fun getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestLocationPermissions()
        } else {
            map.isMyLocationEnabled = true
            fusedLocationClient.lastLocation.addOnCompleteListener {
                val location = it.result
                if (location != null) {
                    val latLng = LatLng(location.latitude, location.longitude)
                    val update = CameraUpdateFactory.newLatLngZoom(latLng, 14.0f)
                    map.moveCamera(update)
                } else {
                    Log.e(TAG, "No location found")
                }
            }
        }
    }

    private fun requestLocationPermissions() {
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_LOCATION
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.size == 1 && grantResults[0] ==
                PackageManager.PERMISSION_GRANTED
            ) {
                getCurrentLocation()
            } else {
                Log.e(TAG, "Location permission denied")
            }
        }
    }
}