package com.example.twitterclone.ui.activities.authorization_activity.authorization_fragments

import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.View.OnTouchListener
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.authorization_activity.AuthorizationActivity
import com.example.twitterclone.ui.activities.twitter_activity.TwitterActivity
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_log_in.*


class LogInFragment : Fragment() {

    private var hiddenInput = true
    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().setTheme(R.style.CreateAccountFragmentTheme)
        val view = inflater.inflate(R.layout.fragment_log_in, container, false)
        inputMethodManager =
            requireActivity().getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AuthorizationActivity?)!!.setSupportActionBar(toolbar)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        signUpTextView.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.openCreateAccountFragment)
        }
        forgotPasswordTextView.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.openForgotPasswordFragment)
        }
        logInFragment.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }


        passwordET.setOnTouchListener(object : OnTouchListener {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (p1?.action == MotionEvent.ACTION_UP) {
                    if (p1.rawX >= passwordET.right - passwordET.compoundDrawables[DRAWABLE_RIGHT].bounds.width()
                    ) {
                        // your action here
                        if (hiddenInput) {
                            passwordET.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                            hiddenInput = false
                        } else {
                            passwordET.inputType =
                                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                            hiddenInput = true
                        }
                        return true
                    }
                }
                return false
            }
        })

        passwordET.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    inputMethodManager.hideSoftInputFromWindow(
                        requireActivity().currentFocus?.windowToken,
                        0
                    )
                    checkValidity()
                    return true
                }
                return false
            }
        })

        logInButton.setOnClickListener {
            checkValidity()
        }

        passwordET.addTextChangedListener(object :TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                errorTextView.visibility = View.GONE
            }

            override fun afterTextChanged(p0: Editable?) {
            }

        })

    }

    private fun checkValidity() {
        val userName = userNameET.text.toString()
        val password = passwordET.text.toString()
        val userNameLength = 15

        if (userName.length in 1..userNameLength && password.isNotEmpty()) {
            userNameTextInputLayout.error = null
            userNameET.error = null
            passwordTextInputLayout.error = null
            passwordET.error = null
            userLogIn(userName, password)
        } else {
            if (userName.isEmpty()) {
                userNameTextInputLayout.error = "Enter a valid username"
                userNameET.error = "Please fill out this field"
            }
            if (password.isEmpty()) {
                passwordTextInputLayout.error = "Enter a valid password"
                passwordET.error = "Please fill out this field"
            }
        }
    }

    private fun userLogIn(userName: String, password: String) {
        logInButton.startAnimation()
        object : CountDownTimer(2000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                ParseUser.logInInBackground(userName, password) { user, e ->
                    if (user != null) {
                        startActivity(Intent(requireActivity(), TwitterActivity::class.java))
                        requireActivity().finish()
                    } else {
                        errorTextView.text = e.message
                        errorTextView.visibility = View.VISIBLE
//                        Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                        logInButton.revertAnimation()
                        logInButton.background = resources.getDrawable(R.drawable.button_style)
                    }
                }
            }
        }.start()
    }

}