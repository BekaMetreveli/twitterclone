package com.example.twitterclone.ui.activities.authorization_activity.authorization_fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import kotlinx.android.synthetic.main.fragment_welcome.*

class WelcomeFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createAccountButton.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.openCreateAccountFragment)
        }
        logInTextView.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.openLogInFragment)
        }
    }


}