package com.example.twitterclone.ui.activities.authorization_activity

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import com.example.twitterclone.ProfileViewModel
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.twitter_activity.TwitterActivity
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_splash_screen.*


class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.AuthorizationActivityTheme)
        setContentView(R.layout.activity_splash_screen)
        init()
    }

    private fun init() {
        object : CountDownTimer(1500, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                if (ParseUser.getCurrentUser() != null) {
                    startActivity(Intent(this@SplashScreenActivity, TwitterActivity::class.java))
                } else {
                    startActivity(Intent(this@SplashScreenActivity, AuthorizationActivity::class.java))
                }
                finish()
            }
        }.start()

    }
}