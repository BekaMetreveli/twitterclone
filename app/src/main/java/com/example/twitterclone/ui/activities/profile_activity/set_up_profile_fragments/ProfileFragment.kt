package com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.ProfileViewModel
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments.*
import com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments.adapters.TabAdapter
import com.parse.ParseFile
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : Fragment(), View.OnClickListener {

    private lateinit var itemView: View
    private lateinit var adapter: TabAdapter
    private lateinit var profileViewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        itemView = view
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel::class.java)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as SetUpProfileActivity?)!!.setSupportActionBar(toolbar)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        setTabLayout()
        readUser()

        setUpProfileTV.setOnClickListener(this)
        profileImageView.setOnClickListener(this)
        coverImageView.setOnClickListener(this)
        profileLocation.setOnClickListener(this)
        profileBio.setOnClickListener(this)
    }

    private fun setTabLayout() {
        adapter = TabAdapter(requireActivity().supportFragmentManager)
        adapter.addFragment(TweetsFragment(), "Tweets")
        adapter.addFragment(FollowersFragment(), "Followers")
        adapter.addFragment(FollowingsFragment(), "Following")
//        adapter.addFragment(MediaFragment(), "Media")

        viewPager.offscreenPageLimit = 4
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun readUser() {
        val userInfo = profileViewModel.getUserProfileInfo(ParseUser.getCurrentUser())
        profileUsername.text = userInfo.username
        profileDateOfJoin.text = userInfo.createdAt
        profileDateOfBirth.text = userInfo.dateOfBirth

        if ((ParseUser.getCurrentUser().get("Address") as String?)?.isNotEmpty() == true) {
            profileLocation.text = userInfo.address
        } else {
            profileLocation.visibility = View.GONE
        }
        if ((ParseUser.getCurrentUser().get("Bio") as String?)?.isNotEmpty() == true) {
            profileBio.text = userInfo.bio
        } else {
            profileBio.visibility = View.GONE
        }
//        profileImageView.setImageBitmap(userInfo.profilePicture)
//        coverImageView.setImageBitmap(userInfo.coverPicture)

        val thisUser = ParseUser.getCurrentUser()
        val profilePicture = thisUser.get("profilePicture") as ParseFile?
        profilePicture?.getDataInBackground { data, e ->
            if (e == null && data != null) {
                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                profileImageView.setImageBitmap(bitmap)
            }
        }

        val coverPicture = thisUser.get("coverPicture") as ParseFile?
        coverPicture?.getDataInBackground { data, e ->
            if (e == null && data != null) {
                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                coverImageView.setImageBitmap(bitmap)
            }
        }
    }

    override fun onClick(p0: View?) {
        when (p0) {
            setUpProfileTV -> NavHostFragment.findNavController(this)
                .navigate(R.id.openPickAProfilePictureFragment)
            profileImageView -> NavHostFragment.findNavController(this)
                .navigate(R.id.openPickAProfilePictureFragment)
            coverImageView -> NavHostFragment.findNavController(this)
                .navigate(R.id.action_profileFragment_to_pickAHeaderFragment)
            profileLocation -> NavHostFragment.findNavController(this)
                .navigate(R.id.action_profileFragment_to_mapsFragment)
            profileBio -> NavHostFragment.findNavController(this)
                .navigate(R.id.action_profileFragment_to_describeYourselfFragment)
        }
    }

}