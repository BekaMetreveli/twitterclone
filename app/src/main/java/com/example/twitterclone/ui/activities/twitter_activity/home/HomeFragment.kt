package com.example.twitterclone.ui.activities.twitter_activity.home

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputType
import android.view.Gravity.CENTER
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.twitterclone.R
import com.parse.*
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var adapter: TweetsRecyclerViewAdapter
    private var users = mutableListOf<String>()
    private var tweets = mutableListOf<String>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fab.setOnClickListener {
            val builder = AlertDialog.Builder(
                requireContext(),
                AlertDialog.THEME_DEVICE_DEFAULT_LIGHT
            )
            builder.setTitle("Send a Tweet")

            val tweetEditText = EditText(requireContext())
            tweetEditText.setBackgroundDrawable(resources.getDrawable(R.drawable.send_tweet_edittext_background))
            tweetEditText.inputType = InputType.TYPE_CLASS_TEXT

            val linearLayout = LinearLayout(requireContext())

            val layoutParams = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            layoutParams.gravity = CENTER

            tweetEditText.hint = "What's on your mind?"
            tweetEditText.setTextColor(resources.getColor(R.color.black))
            tweetEditText.layoutParams = layoutParams

            linearLayout.addView(tweetEditText)
            linearLayout.setPadding(60, 0, 60, 0)

            builder.setView(linearLayout)
            builder.setPositiveButton("Send", object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    if (tweetEditText.text.toString().isNotEmpty()) {
                        saveTweetOnParse(tweetEditText.text.toString())
                    }
                }
            })
            builder.setNegativeButton("Cancel", object : DialogInterface.OnClickListener {
                override fun onClick(p0: DialogInterface?, p1: Int) {
                    p0?.cancel()
                }

            })
            builder.show()
        }

        setAdapter()
        getTweets()

    }

    private fun saveTweetOnParse(tweetText: String) {
        val tweet = ParseObject("Tweet")
        tweet.put("tweets", tweetText)
        tweet.put("influencer", ParseUser.getCurrentUser().username)
        tweet.saveInBackground(object : SaveCallback {
            override fun done(e: ParseException?) {
                if (e == null) {
                    Toast.makeText(requireContext(), "Your tweet was sent.", Toast.LENGTH_SHORT)
                        .show()
                } else {
                    Toast.makeText(requireContext(), "Tweet failed :(", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun setAdapter() {
        adapter = TweetsRecyclerViewAdapter(emptyList(), emptyList())
        tweetsRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        tweetsRecyclerView.adapter = adapter
    }

    private fun getTweets() {
        users.clear()
        tweets.clear()
        val query = ParseQuery.getQuery<ParseObject>("Tweet")
        query.whereContainedIn(
            "influencer",
            ParseUser.getCurrentUser().getList<String>("isFollowing")
        )
        query.orderByDescending("createdAt")
        query.findInBackground(object : FindCallback<ParseObject> {
            override fun done(objects: MutableList<ParseObject>?, e: ParseException?) {
                if (e == null && objects != null) {
                    for (tweet in objects) {
                        users.add(tweet.get("influencer") as String)
                        tweets.add(tweet.get("tweets") as String)
                    }
                    adapter.setAdapterData(users, tweets)

                }
            }
        })

    }


}
