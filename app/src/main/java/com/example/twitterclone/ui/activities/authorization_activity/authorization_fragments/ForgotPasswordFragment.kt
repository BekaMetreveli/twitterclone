package com.example.twitterclone.ui.activities.authorization_activity.authorization_fragments

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.authorization_activity.AuthorizationActivity
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_create_account.*
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import kotlinx.android.synthetic.main.fragment_forgot_password.toolbar


class ForgotPasswordFragment : Fragment() {

    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        requireActivity().setTheme(R.style.CreateAccountFragmentTheme)
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inflater.inflate(R.layout.fragment_forgot_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AuthorizationActivity?)!!.setSupportActionBar(toolbar)
        (activity as AuthorizationActivity?)!!.supportActionBar?.title =
            resources.getString(R.string.change_password)
        (activity as AuthorizationActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        userNameEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    checkIfUsernameExists(view, userNameEditText.text.toString())
                    return true
                }
                return false
            }
        })

        forgotPasswordFragment.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }

        searchButton.setOnClickListener {
            checkIfUsernameExists(view, userNameEditText.text.toString())
        }

    }

    private fun checkIfUsernameExists(view: View, username: String) {
        val query = ParseUser.getQuery()
        query.whereEqualTo("username", username)
        query.findInBackground { results, e ->
            // results has the list of users that match either the email address or username
            if (e == null && results.size > 0) {
                errorTextView.visibility = View.GONE
                val action = ForgotPasswordFragmentDirections.actionForgotPasswordFragmentToUpdatePasswordFragment(username)
                Navigation.findNavController(view).navigate(action)
            } else {
                errorTextView.visibility = View.VISIBLE
            }
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
    }

}