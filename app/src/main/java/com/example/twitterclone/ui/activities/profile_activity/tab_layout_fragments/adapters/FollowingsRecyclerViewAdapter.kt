package com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments.adapters

import android.content.Intent
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.twitterclone.ProfileActivity
import com.example.twitterclone.R
import com.parse.ParseFile
import com.parse.ParseUser
import kotlinx.android.synthetic.main.adapter_followings.view.*

class FollowingsRecyclerViewAdapter(private var users: List<String>) :
    RecyclerView.Adapter<FollowingsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_followings, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount(): Int {
        return users.size
    }

    fun setAdapterData(adapterItems: List<String>) {
        this.users = adapterItems
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var user: String
        fun onBind() {
            user = users[adapterPosition]

            //Set user's Username
            itemView.userName.text = user

            //Set user's profile picture
            val query = ParseUser.getQuery()
            query.whereEqualTo("username", user)
            query.findInBackground { obj, e ->
                if (e == null && obj != null) {
                    for (i in obj) {
                        val file = i.get("profilePicture") as ParseFile?
                        file?.getDataInBackground { data, e ->
                            if (e == null && data != null) {
                                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                                itemView.userImage.setImageBitmap(bitmap)
                            }
                        }
                    }
                }
            }

            //Go to user's profile page
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, ProfileActivity::class.java)
                intent.putExtra("username", user)
                ContextCompat.startActivity(itemView.context, intent, null)
            }
        }
    }

}