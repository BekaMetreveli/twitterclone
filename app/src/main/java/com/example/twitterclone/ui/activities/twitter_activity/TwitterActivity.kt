package com.example.twitterclone.ui.activities.twitter_activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.example.twitterclone.R
import com.example.twitterclone.ui.AboutUsDialog
import com.example.twitterclone.ui.activities.authorization_activity.SplashScreenActivity
import com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments.SetUpProfileActivity
import com.example.twitterclone.ui.activities.twitter_activity.home.HomeFragment
import com.example.twitterclone.ui.activities.twitter_activity.messages.MessagesFragment
import com.example.twitterclone.ui.activities.twitter_activity.search.SearchFragment
import com.parse.ParseUser
import kotlinx.android.synthetic.main.activity_twitter.*
import kotlinx.android.synthetic.main.bottom_navigation.*
import kotlinx.android.synthetic.main.bottom_navigation.fragmentNameTextView
import kotlinx.android.synthetic.main.bottom_navigation.toolbar
import kotlinx.android.synthetic.main.drawer_body.*

class TwitterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_twitter)

        overridePendingTransition(R.anim.zoom_and_fade_in, 0)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            //Open Drawer Menu
            drawerLayout.openDrawer((GravityCompat.START))
        }
        init()
        drawerMenuActions()

    }

    private fun init() {
        val fragments = mutableListOf<Fragment>()
        fragments.add(HomeFragment())
        fragments.add(SearchFragment())
        fragments.add(MessagesFragment())

        viewPagerWidget.adapter = ViewPagerAdapter(supportFragmentManager, fragments)

        viewPagerWidget.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                bottom_nav_view.menu.getItem(position).isChecked = true
                if (position == 1) {
                    searchEditText.visibility = View.VISIBLE
                    toolbarIcon.visibility = View.GONE
                    fragmentNameTextView.visibility = View.GONE
                } else {
                    searchEditText.visibility = View.GONE
                    toolbarIcon.visibility = View.VISIBLE
                    fragmentNameTextView.visibility = View.VISIBLE
                    fragmentNameTextView.text = bottom_nav_view.menu.getItem(position).title
                }

            }

        })

        viewPagerWidget.scrollBarSize = 3
        viewPagerWidget.offscreenPageLimit = 3

        bottom_nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_home -> viewPagerWidget.currentItem = 0
                R.id.navigation_search -> viewPagerWidget.currentItem = 1
                R.id.navigation_messages -> viewPagerWidget.currentItem = 2
            }
            true
        }

    }

    private fun drawerMenuActions() {
        homeActivity.setOnClickListener {
            recreate()
            if (drawerLayout.isDrawerOpen((GravityCompat.START)))
                drawerLayout.closeDrawer((GravityCompat.END))
        }

        profileActivity.setOnClickListener {
            startActivity(Intent(this, SetUpProfileActivity::class.java))
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        }

        aboutUsTV.setOnClickListener {
            val newFragment = AboutUsDialog.newInstance(this)
            newFragment.show(supportFragmentManager, "aboutUsDialog")
        }

        logOut.setOnClickListener {
            ParseUser.logOut()
            startActivity(Intent(this, SplashScreenActivity::class.java))
            finish()
        }

    }

}