package com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.twitterclone.R
import com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments.adapters.FollowersRecyclerViewAdapter
import com.parse.ParseObject
import com.parse.ParseQuery
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_followers.*

class FollowersFragment : Fragment() {

    private lateinit var adapter: FollowersRecyclerViewAdapter
    private var users = mutableListOf<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        getUsers()
    }

    private fun setAdapter() {
        adapter = FollowersRecyclerViewAdapter(emptyList())
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = adapter
    }

    private fun getUsers() {
        var tempUsers: MutableList<String>? = mutableListOf()
        val query = ParseQuery.getQuery<ParseObject>("Followers")
        query.whereEqualTo("influencer", ParseUser.getCurrentUser().username)
        query.findInBackground { obj, e ->

            if (e == null && obj != null) {
                for (i in obj) {
                    tempUsers = i.getList<String>("followers") as MutableList<String>?
                    tempUsers?.let { users.addAll(it) }
                    adapter.setAdapterData(users)
                }
            }
        }

    }
}