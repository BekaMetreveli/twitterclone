package com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_change_password.*
import kotlinx.android.synthetic.main.fragment_describe_yourself.*
import kotlinx.android.synthetic.main.fragment_describe_yourself.nextButton
import kotlinx.android.synthetic.main.fragment_describe_yourself.skipForNowTV
import kotlinx.android.synthetic.main.fragment_describe_yourself.toolbar

class DescribeYourselfFragment : Fragment() {

    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inflater.inflate(R.layout.fragment_describe_yourself, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as SetUpProfileActivity?)!!.setSupportActionBar(toolbar)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )

        skipForNowTV.setOnClickListener {
            NavHostFragment.findNavController(this)
                .navigate(R.id.openWhatShouldPeopleCallYouFragment)
        }

        val parseBio = ParseUser.getCurrentUser().get("Bio") as String?
        if (parseBio?.isNotEmpty() == true) {
            bioEditText.setText(parseBio)
        }

        bioEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    val currentBio = bioEditText.text.toString()
                    if (currentBio != parseBio) {
                        saveBioOnParse(currentBio)
                    } else {
                        NavHostFragment.findNavController(this@DescribeYourselfFragment)
                            .navigate(R.id.openWhatShouldPeopleCallYouFragment)
                    }
                    return true
                }
                return false
            }
        })

        nextButton.setOnClickListener {
            val currentBio = bioEditText.text.toString()
            if (currentBio != parseBio) {
                saveBioOnParse(currentBio)
            } else {
                NavHostFragment.findNavController(this)
                    .navigate(R.id.openWhatShouldPeopleCallYouFragment)
            }
        }

        describeYourselfFragment.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }

    }

    private fun saveBioOnParse(bio: String) {
        val obj = ParseUser.getCurrentUser()
        obj.put("Bio", bio)

        obj.saveInBackground { e ->
            if (e == null) {
                Toast.makeText(requireActivity(), "Bio was updated.", Toast.LENGTH_SHORT)
                    .show()
                NavHostFragment.findNavController(this)
                    .navigate(R.id.openWhatShouldPeopleCallYouFragment)
            } else {
                Toast.makeText(requireActivity(), "Couldn't update bio.", Toast.LENGTH_SHORT).show()
            }
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
    }
}