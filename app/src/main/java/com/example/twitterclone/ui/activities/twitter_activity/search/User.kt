package com.example.twitterclone.ui.activities.twitter_activity.search

class User(
    val username: String?,
    val dateOfBirth: String?,
    val bio: String? = "",
    val address: String?,
)
