package com.example.twitterclone.ui.activities.twitter_activity.search

import android.content.Intent
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.example.twitterclone.ProfileActivity
import com.example.twitterclone.R
import com.parse.*
import kotlinx.android.synthetic.main.adapter_user.view.*

class UsersRecyclerViewAdapter(
    private var users: List<User>
) :
    RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_user, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount(): Int {
        return users.size
    }

    fun setAdapterData(adapterItems: List<User>) {
        this.users = adapterItems
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var user: User
        fun onBind() {
            user = users[adapterPosition]

            //Set user's Username
            itemView.userName.text = user.username

            //Set user's Address
            if (user.address?.isNotEmpty() == true) {
                itemView.userLocation.text = user.address
            } else {
                itemView.userLocation.visibility = View.GONE
            }

            //Set user's Bio
            if (user.bio?.isNotEmpty() == true) {
                itemView.userBio.text = user.bio
            } else {
                itemView.userBio.visibility = View.GONE
            }

            //Set user's profile picture
            val query = ParseUser.getQuery()
            query.whereEqualTo("username", user.username)
            query.findInBackground { obj, e ->
                if (e == null && obj != null) {
                    for (i in obj) {
                        val file = i.get("profilePicture") as ParseFile?
                        file?.getDataInBackground { data, e ->
                            if (e == null && data != null) {
                                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                                itemView.userImage.setImageBitmap(bitmap)
                            }
                        }
                    }
                }
            }

            //Follow button functionality
            for (i in users) {
                if (ParseUser.getCurrentUser().getList<String>("isFollowing")?.contains(user.username) == true) {
                    itemView.followButton.tag = "1"
                }
            }
            itemView.followButton.apply {
                if (this.tag == "0") {
                    text = itemView.resources.getString(R.string.follow_Capital)
                    setBackgroundDrawable(itemView.resources.getDrawable(R.drawable.follow_button))
                    setTextColor(itemView.resources.getColor(R.color.input_texts_color))
                } else {
                    text = itemView.resources.getString(R.string.following_Capital)
                    setBackgroundDrawable(itemView.resources.getDrawable(R.drawable.following_button))
                    setTextColor(itemView.resources.getColor(R.color.white))
                }
                setOnClickListener {
                    if (this.tag == "0") {
                        text = itemView.resources.getString(R.string.following_Capital)
                        setBackgroundDrawable(itemView.resources.getDrawable(R.drawable.following_button))
                        setTextColor(itemView.resources.getColor(R.color.white))
                        tag = "1"
                        ParseUser.getCurrentUser().add("isFollowing", user.username)
                        //Add current user to selected user's follower's list.
                        val qry = ParseQuery.getQuery<ParseObject>("Followers")
                        qry.whereEqualTo("influencer", user.username)
                        qry.findInBackground { obj, e ->
                            if (e == null && obj != null) {
                                for (i in obj) {
                                    i.add("followers", ParseUser.getCurrentUser().username)
                                    i.saveInBackground { e ->
                                        if (e == null) {
                                            Log.i("New Follower", "Added")
                                        } else {
                                            Toast.makeText(itemView.context, "Error", Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        text = itemView.resources.getString(R.string.follow_Capital)
                        setBackgroundDrawable(itemView.resources.getDrawable(R.drawable.follow_button))
                        setTextColor(itemView.resources.getColor(R.color.input_texts_color))
                        tag = "0"
                        ParseUser.getCurrentUser().getList<String>("isFollowing")!!.remove(user.username)
                        val tempUsers = ParseUser.getCurrentUser().getList<String>("isFollowing")
                        ParseUser.getCurrentUser().remove("isFollowing")
                        if (tempUsers != null) {
                            ParseUser.getCurrentUser().put("isFollowing", tempUsers)
                        }
                        //Remove current user to selected user's follower's list.
                        val qry = ParseQuery.getQuery<ParseObject>("Followers")
                        qry.whereEqualTo("influencer", user.username)
                        qry.findInBackground { obj, e ->
                            if (e == null && obj != null) {
                                for (i in obj) {
                                    i.getList<String>("followers")!!.remove(ParseUser.getCurrentUser().username)
                                    val tempFollowers = i.getList<String>("followers")
                                    i.remove("followers")
                                    if (tempFollowers != null) {
                                        i.put("followers", tempFollowers)
                                    }
                                    i.saveInBackground { e ->
                                        if (e == null) {
                                            Log.i("Follower", "Removed")
                                        } else {
                                            Toast.makeText(itemView.context, "Error", Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ParseUser.getCurrentUser().saveInBackground()
                }
            }

            //Go to user's profile page
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, ProfileActivity::class.java)
                intent.putExtra("username", user.username)
                ContextCompat.startActivity(itemView.context, intent, null)
            }
        }
    }
}