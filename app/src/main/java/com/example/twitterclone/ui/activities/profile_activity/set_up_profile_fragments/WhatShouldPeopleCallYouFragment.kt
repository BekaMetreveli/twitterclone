package com.example.twitterclone.ui.activities.profile_activity.set_up_profile_fragments

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.navigation.fragment.NavHostFragment
import com.example.twitterclone.R
import com.parse.ParseUser
import kotlinx.android.synthetic.main.fragment_what_should_people_call_you.*

class WhatShouldPeopleCallYouFragment : Fragment() {

    private lateinit var inputMethodManager: InputMethodManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        return inflater.inflate(R.layout.fragment_what_should_people_call_you, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as SetUpProfileActivity?)!!.setSupportActionBar(toolbar)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as SetUpProfileActivity?)!!.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            requireActivity().onBackPressed()
        }

        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )

        skipForNowTV.setOnClickListener {
            NavHostFragment.findNavController(this).navigate(R.id.openMapsFragment)
        }

        val parseUsername = ParseUser.getCurrentUser().get("username") as String
        if (parseUsername.isNotEmpty()) {
            userNameEditText.setText(parseUsername)
        }

        userNameEditText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                // If the event is a key-down event on the "enter" button
                if (event.action == KeyEvent.ACTION_DOWN &&
                    keyCode == KeyEvent.KEYCODE_ENTER
                ) {
                    val currentUsername = userNameEditText.text.toString()
                    if (currentUsername.isNotEmpty() && currentUsername != currentUsername) {
                        saveUserNameOnParse(currentUsername)
                    } else {
                        NavHostFragment.findNavController(this@WhatShouldPeopleCallYouFragment).navigate(R.id.openMapsFragment)
                    }
                    return true
                }
                return false
            }
        })

        nextButton.setOnClickListener {
            val currentUsername = userNameEditText.text.toString()
            if (currentUsername.isNotEmpty() && currentUsername != currentUsername) {
                saveUserNameOnParse(currentUsername)
            } else {
                NavHostFragment.findNavController(this).navigate(R.id.openMapsFragment)
            }
        }

        whatShouldPeopleCallYouFragment.setOnClickListener {
            inputMethodManager.hideSoftInputFromWindow(
                requireActivity().currentFocus?.windowToken,
                0
            )
        }

    }

    private fun saveUserNameOnParse(username: String) {
        val obj = ParseUser.getCurrentUser()
        obj.put("username", username)
        obj.saveInBackground { e ->
            if (e == null) {
                Toast.makeText(requireActivity(), "Username was updated", Toast.LENGTH_SHORT)
                    .show()
                NavHostFragment.findNavController(this).navigate(R.id.openMapsFragment)
            } else {
                Toast.makeText(requireActivity(), e.message, Toast.LENGTH_SHORT).show()
            }
        }
        inputMethodManager.hideSoftInputFromWindow(
            requireActivity().currentFocus?.windowToken,
            0
        )
    }

}