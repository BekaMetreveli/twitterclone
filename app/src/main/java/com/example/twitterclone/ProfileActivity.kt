package com.example.twitterclone

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.twitterclone.ui.activities.profile_activity.tab_layout_fragments.adapters.TabAdapter


class ProfileActivity : AppCompatActivity() {

    private lateinit var adapter: TabAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
//        setToolbar()
//        setTabLayout()
//        readObject()
//        setUpProfileTV.setOnClickListener {
//            startActivity(Intent(this, SetUpProfileActivity::class.java))
//        }

    }

//    private fun readObject() {
//        val thisUser = ParseUser.getCurrentUser()
//        profileUsername.text = thisUser.username
//        profileDateOfJoin.text = "Created at: ${thisUser.createdAt.date}/${thisUser.createdAt.month + 1}/${thisUser.createdAt.toString().substring(thisUser.createdAt.toString().length - 4)}"
//
//        if (thisUser.get("dateOfBirth") as String == "Date of birth") {
//            profileDateOfBirth.visibility = View.GONE
//        } else {
//            profileDateOfBirth.text = "Date of birth: ${thisUser.get("dateOfBirth")}"
//        }
//
//        profileBio.text = thisUser.get("Bio") as String
//
//        val profilePicture = thisUser.get("profilePicture") as ParseFile?
//        profilePicture?.getDataInBackground { data, e ->
//            if (e == null && data != null) {
//                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
//                profileImageView.setImageBitmap(bitmap)
//                Log.i("image", "done")
//            }
//        }
//        val coverPicture = thisUser.get("coverPicture") as ParseFile?
//        coverPicture?.getDataInBackground { data, e ->
//            if (e == null && data != null) {
//                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
//                coverImageView.setImageBitmap(bitmap)
//            }
//        }
//    }
//
//    private fun setToolbar() {
//        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayShowTitleEnabled(false)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        toolbar.setNavigationOnClickListener {
//            onBackPressed()
//        }
//    }
//
//    private fun setTabLayout() {
//        adapter = TabAdapter(supportFragmentManager)
//        adapter.addFragment(TweetsFragment(), "Tweets")
//        adapter.addFragment(FollowingsFragment(), "Following")
//        adapter.addFragment(MediaFragment(), "Media")
//
//        viewPager.adapter = adapter
//        tabLayout.setupWithViewPager(viewPager)
//
//    }
//
//    override fun onBackPressed() {
//        super.onBackPressed()
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
//    }
}