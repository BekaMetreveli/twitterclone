package com.example.twitterclone

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.lifecycle.AndroidViewModel
import com.parse.ParseFile
import com.parse.ParseUser

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    fun getUserProfileInfo(user: ParseUser): UserProfileViewData {
        return UserProfileViewData(
            user.username,
            "Date of birtch: ${user.get("dateOfBirth")}",
            "Created at: ${user.createdAt.date}/${user.createdAt.month + 1}/${user.createdAt.toString().substring(user.createdAt.toString().length - 4)}",
            getUserProfilePicture(user),
            getUserCoverPicture(user),
            "Bio: ${user.get("Bio")}",
            "Address: ${user.get("Address")}"
        )
    }

    private fun getUserProfilePicture(user: ParseUser): Bitmap? {
        val profilePicture = user.get("profilePicture") as ParseFile?
        var profilePictureBitmap: Bitmap? = null
        profilePicture?.getDataInBackground { data, e ->
            if (e == null && data != null) {
                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                profilePictureBitmap = bitmap
            }
        }
        return profilePictureBitmap
    }

    private fun getUserCoverPicture(user: ParseUser): Bitmap? {
        val coverPicture = user.get("coverPicture") as ParseFile?
        var coverPictureBitmap: Bitmap? = null
        coverPicture?.getDataInBackground { data, e ->
            if (e == null && data != null) {
                val bitmap = BitmapFactory.decodeByteArray(data, 0, data.size)
                coverPictureBitmap = bitmap
            }
        }
        return coverPictureBitmap
    }

    data class UserProfileViewData(
        val username: String = "",
        val dateOfBirth: String = "",
        val createdAt: String = "",
        val profilePicture: Bitmap? = null,
        val coverPicture: Bitmap? = null,
        val bio: String = "",
        val address: String = "",
    )
}